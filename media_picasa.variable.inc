<?php


/**
 * Implements hook_variable_info().
 */
function media_picasa_variable_info($options) {

  $variable['media_picasa_username'] = array(
    'access' => 'administer media_picasa',
    'title' => t('Picasa Username', array(), $options),
    'type' => 'string',
  );

  return $variable;
}


