# Media Picasa - Integrate the picasa image selector into your site

## Description--

 -Media Picasa lets you connect your site from your picasa albums using your username.  

This module lets you Adds a 'Picasa album' tab to the media dialog so that you can easily browse images from  the Media Selector widget.  After you have selected your image, it is copied over to your Files directory so you can still apply Drupal file styles and access control.

## Installation--  

Install as you would any other Drupal module:

1. Download the module and put it in sites/all/modules or sites/SITENAME/modules
3. Go to admin/config/media/picasa and enter your picasa username.
4. Enable media picasa in your media field settings under "Enabled browser plugins"


## Who--

Developed and maintained by Kalyani Sehgal.

